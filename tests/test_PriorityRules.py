import dedupe
from dedupe import SortStyle, PriorityRules, DuplicateSet

PREFERRED_FOLDER = "/home/jim/timeline"

file_1 = dedupe.makeFileDetail("/path/a/a.jpg")
file_2 = dedupe.makeFileDetail("/path/a/b.jpg")
file_3 = dedupe.makeFileDetail("/path/b/a.jpg")
file_4 = dedupe.makeFileDetail("/path/b/b.jpg")

file_pfs_1 = dedupe.makeFileDetail("/home/jim/path/IMG_20190611_135738.jpg", PREFERRED_FOLDER)
file_pfs_2 = dedupe.makeFileDetail("/home/jim/path/IMG_20190611_135738(1).jpg", PREFERRED_FOLDER)
file_pfs_3 = dedupe.makeFileDetail("/home/jim/timeline/IMG_20190611_135738.jpg", PREFERRED_FOLDER)
file_pfs_4 = dedupe.makeFileDetail("/home/jim/timeline/IMG_20190611_13573.jpg", PREFERRED_FOLDER)

file_pfn_1 = dedupe.makeFileDetail("/home/jim/google/Photos/takeout-jim/Photos from 2015/IMG_9530.JPG")
file_pfn_2 = dedupe.makeFileDetail("/home/jim/google/Photos/takeout-jim/2015 - TC/IMG_9530.JPG")


def test_PreferAlbums_1():
    startList = [file_pfn_1, file_pfn_2]
    rules = PriorityRules(preferGoogleAlbums=True, preferShorterName=True)
    expectedResult = DuplicateSet(file_pfn_2, [file_pfn_1])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_PreferShorterName_1():
    startList = [file_pfs_1, file_pfs_2]
    rules = PriorityRules(preferredFolder=PREFERRED_FOLDER, preferShorterName=True)

    expectedResult = DuplicateSet(file_pfs_1, [file_pfs_2])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_PreferShorterName_2():
    startList = [file_pfs_2, file_pfs_1]
    rules = PriorityRules(preferredFolder=PREFERRED_FOLDER, preferShorterName=True)

    expectedResult = DuplicateSet(file_pfs_1, [file_pfs_2])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_PreferredFolder_1():
    startList = [file_pfs_1, file_pfs_3]
    rules = PriorityRules(preferredFolder=PREFERRED_FOLDER, preferShorterName=True)

    expectedResult = DuplicateSet(file_pfs_3, [file_pfs_1])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_PreferredFolder_2():
    startList = [file_pfs_3, file_pfs_4]
    rules = PriorityRules(preferredFolder=PREFERRED_FOLDER)

    expectedResult = DuplicateSet(file_pfs_3, [file_pfs_4])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_sorting_1():
    startList = [file_1, file_2, file_3, file_4]
    rules = PriorityRules(sortFolders=SortStyle(True, False))

    expectedResult = DuplicateSet(file_1, [file_2, file_3, file_4])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_sorting_2():
    startList = [file_1, file_2, file_3, file_4]
    rules = PriorityRules(sortFiles=SortStyle(True, False))

    expectedResult = DuplicateSet(file_1, [file_3, file_2, file_4])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult


def test_sorting_3():
    startList = [file_4, file_3, file_1, file_2]
    rules = PriorityRules(sortFiles=SortStyle(True, False), sortFolders=SortStyle(True, False))

    expectedResult = DuplicateSet(file_1, [file_2, file_3, file_4])
    actualResult = dedupe.organizeDuplicates(startList, rules)

    assert expectedResult == actualResult
