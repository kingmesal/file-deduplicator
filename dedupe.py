#!/usr/bin/env python
"""
Fast duplicate file finder.
Usage: duplicates.py <folder> [<folder>...]
Based on https://stackoverflow.com/a/36113168/300783
Modified for Python3 with some small code improvements.
"""
from io import BufferedReader
import os
import sys
import re
from enum import Enum, auto
import getopt
import hashlib
from typing import NamedTuple, Optional, List, DefaultDict, Any
from collections import defaultdict
import logging.config
import logging
import piexif
from piexif import ImageIFD, ExifIFD


GOOGLE_YEAR_PHOTOS_PATTERN = ".*/Photos from [0-9]{4}.*$"
logger = logging.getLogger()
tagImageUniqueID = 42016


class Approach(Enum):
    SIZE = auto()
    EXIF = auto()


class SortStyle(NamedTuple):
    sort: bool
    reverse: bool = False


class PriorityRules(NamedTuple):
    sortFolders: Optional[SortStyle] = None
    sortFiles: Optional[SortStyle] = None
    preferredFolder: Optional[str] = None
    preferShorterName: Optional[bool] = None
    preferGoogleAlbums: Optional[bool] = None


class FileDetail(NamedTuple):
    fullPath: str
    folder: str
    baseFileName: str
    extension: str
    fileName: str
    size: int
    isInPreferredFolder: Optional[bool] = None
    isInGoogleYearFolder: Optional[bool] = None
    exifHash: str = None
    xPlusY: int = None


class DuplicateSet(NamedTuple):
    originalFile: FileDetail
    duplicates: List[FileDetail]


def makeFileDetail(fullPath: str, preferredFolder: str = None, addExif: bool = False):
    fileSize = 0 if not os.path.exists(fullPath) else os.path.getsize(fullPath)
    base, ext = os.path.splitext(fullPath)
    folder, baseFileName = os.path.split(base)
    fileName = baseFileName + ext
    ext = ext.replace(".", "").lower()
    inPreferred = False if preferredFolder is None else fullPath.startswith(preferredFolder)
    inYears = re.match(GOOGLE_YEAR_PHOTOS_PATTERN, folder) is not None
    exifHash = None
    xPlusY = 0
    if addExif:
        try:
            exifDict = piexif.load(fullPath)
            if exifDict is not None:
                exifDict.pop("1st")
                exifDict.pop("thumbnail")
                try:
                    # For some reason google will flip x and y and modify the interop
                    # tag so we combine the x and y and track it and drop the other
                    # tags that google for some reason touches
                    x = exifDict["Exif"].pop(ExifIFD.PixelXDimension)
                    y = exifDict["Exif"].pop(ExifIFD.PixelYDimension)
                    xPlusY = x + y
                    exifDict["Exif"].pop(ExifIFD.InteroperabilityTag)
                    exifDict["0th"].pop(ImageIFD.PrintImageMatching)
                    exifDict["Exif"].pop(ExifIFD.MakerNote)
                    IUID = str(exifDict["Exif"].pop(tagImageUniqueID), "UTF-8")
                except Exception as e:
                    IUID = None

                exifHash = getStringHash(piexif.dump(exifDict))
                logger.debug("File: %s, hash: %s, xPlusY: %s, iuid: %s", fullPath, exifHash, xPlusY, IUID)
        except Exception as e:
            logger.debug("Problem with loading exif data from file: %s", fullPath)

    return FileDetail(fullPath, folder, baseFileName, ext, fileName, fileSize, inPreferred, inYears, exifHash, xPlusY)


def sizeof(num: int):
    for unit in [" ", " k", " m", " g", " t"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}b"
        num /= 1024.0
    return f"{num:.1f}pb"


def wastedSpace(duplicates: List[FileDetail]):
    total = 0
    for file in duplicates:
        total += file.size
    return total


def removeDuplicates(duplicates: List[FileDetail]):
    for file in duplicates:
        os.remove(file.fullPath)


def chunkReader(fobj: BufferedReader, chunkSize: int = 1024):
    """Generator that reads a file in chunks of bytes"""
    while True:
        chunk = fobj.read(chunkSize)
        if not chunk:
            return
        yield chunk


def getStringHash(exif: bytes):
    hashObj = hashlib.sha1(exif)
    hexDig = hashObj.hexdigest()
    return hexDig


def getHash(fileName: str, firstChunkOnly: bool = False, hashAlgorithm=hashlib.sha1):
    hashobj = hashAlgorithm()
    with open(fileName, "rb") as f:
        if firstChunkOnly:
            hashobj.update(f.read(1024))
        else:
            for chunk in chunkReader(f):
                hashobj.update(chunk)
    return hashobj.digest()


def getShortestNameEntry(fileList: List[FileDetail], usePreferredFolder=False, useGoogleAlbum=False):
    shortestLength = sys.maxsize
    shortestIndex = -1
    for idx, file in enumerate(fileList):
        length = len(file.fileName)
        if usePreferredFolder:
            if file.isInPreferredFolder and length < shortestLength:
                shortestIndex = idx
                shortestLength = length
        elif useGoogleAlbum:
            if file.isInGoogleYearFolder == False and length < shortestLength:
                shortestIndex = idx
                shortestLength = length
        elif length < shortestLength:
            shortestIndex = idx
            shortestLength = length

    return fileList[shortestIndex]


def findFirstInPreferredFolder(fileList: List[FileDetail]):
    first = None
    for file in fileList:
        if file.isInPreferredFolder:
            first = file
            break

    return first


def findFirstInAlbumFolder(fileList: List[FileDetail]):
    first = None
    for file in fileList:
        if file.isInGoogleYearFolder == False:
            first = file
            break

    return first


def organizeDuplicates(fileList: List[FileDetail], rules: PriorityRules = None):
    keep = None
    if PriorityRules is not None:
        if rules.sortFiles is not None and rules.sortFiles.sort:
            fileList.sort(key=lambda x: x.fileName, reverse=rules.sortFiles.reverse)

        if rules.sortFolders is not None and rules.sortFolders.sort:
            fileList.sort(key=lambda x: x.folder, reverse=rules.sortFolders.reverse)

        count = len(fileList)
        useShort = rules.preferShorterName is not None and rules.preferShorterName
        usePreferred = rules.preferredFolder is not None and rules.preferredFolder != ""
        useAlbum = rules.preferGoogleAlbums is not None and rules.preferGoogleAlbums
        if useShort and usePreferred:
            totalInFolder = len(list(filter(lambda x: x.isInPreferredFolder == True, fileList)))
            if totalInFolder > 0 and totalInFolder < count:
                keep = getShortestNameEntry(fileList, usePreferredFolder=True)
                logger.debug("Found shortest name file in preferred folder: %s", keep)
        elif useShort and useAlbum:
            totalInFolder = len(list(filter(lambda x: x.isInGoogleYearFolder == True, fileList)))
            if totalInFolder > 0 and totalInFolder < count:
                keep = getShortestNameEntry(fileList, useGoogleAlbum=True)
                logger.debug("Found shortest name file in album folder: %s", keep)

        if keep is None:
            if useShort:
                keep = getShortestNameEntry(fileList)
                logger.debug("File with shortest name: %s", keep)
            elif usePreferred:
                keep = findFirstInPreferredFolder(fileList)
                logger.debug("First file in preferred folder: %s", keep)
            elif useAlbum:
                keep = findFirstInAlbumFolder(fileList)
                logger.debug("First file in album folder: %s", keep)

    if keep is not None:
        fileList.remove(keep)
        dupSet = DuplicateSet(keep, fileList)
    else:
        logger.debug("Setup default duplicate set")
        dupSet = DuplicateSet(fileList[0], fileList[1:])

    return dupSet


def handleDuplicateList(duplicates: List[FileDetail], removeDups: bool = False, rules: PriorityRules = None):
    logger.info("Handling %s duplicate sets", len(duplicates))
    totalWastedSpace = 0
    totalDuplicateFiles = 0
    # Now, print a summary of all files that share a full hash
    for files in duplicates:
        filesInSet = len(files)
        if filesInSet >= 2:
            # More than one file share the same full hash
            # now it is time to print out a report
            matchedSet = organizeDuplicates(files, rules)

            size = wastedSpace(matchedSet.duplicates)
            totalDuplicateFiles += filesInSet - 1
            totalWastedSpace += size
            logger.info("Duplicates found (wasted space: %s):", sizeof(size))
            logger.info("+ '%s'", matchedSet.originalFile.fullPath)
            for file in matchedSet.duplicates:
                logger.info("- '%s'", file.fullPath)

            if removeDups:
                removeDuplicates(matchedSet.duplicates)

    logger.info(
        "Total duplicate files: %s, space to recover upon removing: %s", totalDuplicateFiles, sizeof(totalWastedSpace)
    )


def buildFileSet(paths: List[str], approach: Approach, extensions: List[str] = None, rules: PriorityRules = None):
    logger.info("Building file set with approach: %s", approach)
    baseSet: DefaultDict[Any, List[FileDetail]] = defaultdict(list)
    for path in paths:
        logger.info("Top Level: %s", path)
        for dirpath, _, filenames in os.walk(path):
            logger.debug("checking: %s", dirpath)
            for file in filenames:
                fullPath = os.path.join(dirpath, file)
                # if the target is a symlink (soft one), this will
                # dereference it - change the value to the actual target file
                fullPath = os.path.realpath(fullPath)

                try:
                    fileDetails = makeFileDetail(fullPath, rules.preferredFolder, approach is Approach.EXIF)

                    # based on the extensions passed in we decide
                    # whether to track a particular file
                    ext = fileDetails.extension
                    if extensions is None or (ext != "" and ext in (extensions)):
                        if approach is Approach.SIZE:
                            baseSet[fileDetails.size].append(fileDetails)
                        elif approach is Approach.EXIF and fileDetails.exifHash is not None:
                            baseSet[fileDetails.exifHash].append(fileDetails)

                except:
                    logger.debug("Encountered a problem with file: %s", fullPath)

    return baseSet


def checkForDuplicates(
    paths: List[str],
    approach: Approach,
    extensions: List[str] = None,
    removeDups: bool = False,
    rules: PriorityRules = None,
):
    baseSet = buildFileSet(paths, approach, extensions, rules)

    if approach is Approach.EXIF:
        logger.info("Total sets being tracked after %s check: %s", approach, len(baseSet))
        filesWithMatchingEXIF: DefaultDict[str, List[FileDetail]] = defaultdict(list)
        for hash, files in baseSet.items():
            files.sort(key=lambda x: x.baseFileName)
            if len(files) >= 2:
                f = files.pop(0)
                filesWithMatchingEXIF[hash].append(f)
                for file in files:
                    #                    if file.xPlusY == f.xPlusY:
                    # if file.baseFileName.startswith(f.baseFileName) and file.xPlusY == f.xPlusY:
                    filesWithMatchingEXIF[hash].append(file)

        handleDuplicateList(filesWithMatchingEXIF.values(), removeDups, rules)

    elif approach is Approach.SIZE:
        logger.info("Total files being tracked after %s check: %s", approach, len(baseSet))

        filesBySmallHash: DefaultDict[(int, bytes), List[FileDetail]] = defaultdict(list)
        filesByFullHash: DefaultDict[bytes, List[FileDetail]] = defaultdict(list)
        # For all files with the same file size, get their hash on the first 1024 bytes
        for fileSize, files in baseSet.items():
            if len(files) >= 2:
                for file in files:
                    try:
                        smallHash = getHash(file.fullPath, firstChunkOnly=True)
                        filesBySmallHash[(fileSize, smallHash)].append(file)
                    except OSError:
                        # the file access might've changed till the exec point got here
                        continue

        logger.info("Total groups of files being tracked after small hash: %s", len(filesBySmallHash))
        # For all files with the hash on the first 1024 bytes, get their hash on the full
        # file - collisions will be duplicates
        for files in filesBySmallHash.values():
            if len(files) >= 2:
                for file in files:
                    try:
                        fullHash = getHash(file.fullPath, firstChunkOnly=False)

                        # Add this file to the list of others sharing the same full hash
                        filesByFullHash[fullHash].append(file)
                    except OSError:
                        # the file access might've changed till the exec point got here
                        continue

        handleDuplicateList(filesByFullHash.values(), removeDups, rules)


def usage():
    print(f"Usage: {sys.argv[0]} [-h <print usage>] [-r <remove duplicates>] folder [<folder>...]")


if __name__ == "__main__":
    loggingConfigFile = "logging.conf"

    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hr",
            [
                "approach=",
                "preferFilesInFolder=",
                "sortFiles=",
                "sortFolders=",
                "preferGoogleAlbums",
                "preferShorterNames",
                "logging",
                "removeDuplicates",
                "help",
                "extList=",
            ],
        )

        paths = None
        extensions = None
        deleteDuplicates = False
        approach = None
        fileSorting = None
        folderSorting = None
        preferAlbums = None
        preferShortNames = None
        preferFolder = None
        for opt, arg in opts:
            if opt in ("--help", "-h"):
                usage()
                sys.exit()
            elif opt in ("--removeDuplicates", "-r"):
                logger.debug("Duplicate files will be deleted from the system")
                deleteDuplicates = True
            elif opt in ("--extList"):
                extensions = arg.lower().split(",")
                logger.debug("Only procoess files with extensions '%s'", extensions)
            elif opt in ("--logging"):
                loggingConfigFile = arg
                logger.debug("Logging configuration will be loaded from: '%s'", loggingConfigFile)
            elif opt in ("--sortFiles"):
                if arg.lower().startswith("asc"):
                    logger.debug("Sort files ascending")
                    fileSorting = SortStyle(True, False)
                elif arg.lower().startswith("desc"):
                    logger.debug("Sort files descending")
                    fileSorting = SortStyle(True, True)
            elif opt in ("--sortFolders"):
                if arg.lower().startswith("asc"):
                    logger.debug("Sort folders ascending")
                    folderSorting = SortStyle(True, False)
                elif arg.lower().startswith("desc"):
                    logger.debug("Sort folders descending")
                    folderSorting = SortStyle(True, True)
            elif opt in ("--preferFilesInFolder"):
                logger.debug("Prefer keepings files in folder: '%s'", preferFolder)
                preferFolder = arg
            elif opt in ("--preferGoogleAlbums"):
                logger.debug("Prefer keeping files in google albums")
                preferAlbums = True
            elif opt in ("--preferShorterNames"):
                logger.debug("Prefer keeping files with shorter names")
                preferShortNames = True
            elif opt in ("--approach"):
                try:
                    approach = Approach[arg.upper()]
                except:
                    continue

        #
        # Setup logging before doing any actual work
        #
        if not os.path.exists(loggingConfigFile):
            print(f"Logging configuration file does not exist: {loggingConfigFile}")
            sys.exit(3)

        logging.config.fileConfig(loggingConfigFile)
        logger = logging.getLogger("dedupe")

        rules = PriorityRules(
            sortFolders=folderSorting,
            sortFiles=fileSorting,
            preferredFolder=preferFolder,
            preferShorterName=preferShortNames,
            preferGoogleAlbums=preferAlbums,
        )

        if approach == None:
            print(f"A deduplication approach must be specified with --approach={Approach._member_names_}")
            usage()
            sys.exit(4)

        paths = args
        if paths is not None and paths != "":
            checkForDuplicates(paths, approach, extensions, deleteDuplicates, rules)
        else:
            usage()
            sys.exit(1)

    except getopt.GetoptError:
        usage()
        sys.exit(2)
