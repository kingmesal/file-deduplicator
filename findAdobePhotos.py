#!/usr/bin/env python
"""
Fast duplicate file finder.
Usage: duplicates.py <folder> [<folder>...]
Based on https://stackoverflow.com/a/36113168/300783
Modified for Python3 with some small code improvements.
"""
import os
import sys
import getopt
from typing import NamedTuple, List
import logging.config
import logging
from libxmp import XMPFiles
import shutil

logger = logging.getLogger()


class CopyDetails(NamedTuple):
    src: str
    dest: str


def isAdobePhoto(fullPath: str, extensions: List[str] = None):
    hasDocId = False
    hasODocId = False
    hasIId = False
    try:
        xmpfile = XMPFiles(file_path=fullPath)
        xmp = xmpfile.get_xmp()
        for item in xmp:
            if item[1] == "xmpMM:DocumentID":
                hasDocId = True
            if item[1] == "xmpMM:OriginalDocumentID":
                hasODocId = True
            if item[1] == "xmpMM:InstanceID":
                hasIId = True

    except Exception as e:
        logger.debug("Could not load xmp data from file: %s", fullPath)

    return hasDocId and hasODocId and hasIId


def buildFileSet(paths: List[str], copyToPath: str = None, extensions: List[str] = None):
    destDir = ""
    if copyToPath is None or copyToPath == "" or not os.path.exists(copyToPath):
        destDir = copyToPath + "/"

    logger.info("Building file set of stock photos")
    stockList: List[CopyDetails] = []
    for path in paths:
        logger.info("Top Level: %s", path)
        for dirpath, _, filenames in os.walk(path):
            logger.debug("checking: %s", dirpath)
            for file in filenames:
                base, ext = os.path.splitext(file)
                _, baseFileName = os.path.split(base)
                fileName = baseFileName + ext
                ext = ext.replace(".", "").lower()
                if extensions is None or (ext != "" and ext in (extensions)):
                    fullPath = os.path.join(dirpath, file)
                    # if the target is a symlink (soft one), this will
                    # dereference it - change the value to the actual target file
                    fullPath = os.path.realpath(fullPath)

                    isStock = isAdobePhoto(fullPath, extensions)
                    if isStock:
                        t = CopyDetails(fullPath, destDir + fileName)
                        stockList.append(t)

    for image in stockList:
        if destDir == "":
            logger.info("Found file '%s'", image.src)
        else:
            logger.info("Copying '%s' to '%s'", image.src, image.dest)
            shutil.copyfile(image.src, image.dest)


if __name__ == "__main__":
    loggingConfigFile = "logging.conf"
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "",
            [
                "copyToPath=",
                "logging",
                "help",
                "extList=",
            ],
        )

        destPath = None
        for opt, arg in opts:
            if opt in ("--copyToPath"):
                destPath = arg
            elif opt in ("--extList"):
                extensions = arg.lower().split(",")
                logger.debug("Only procoess files with extensions '%s'", extensions)
            elif opt in ("--logging"):
                loggingConfigFile = arg
                logger.debug("Logging configuration will be loaded from: '%s'", loggingConfigFile)

        #
        # Setup logging before doing any actual work
        #
        if not os.path.exists(loggingConfigFile):
            print(f"Logging configuration file does not exist: {loggingConfigFile}")
            sys.exit(3)

        logging.config.fileConfig(loggingConfigFile)
        logger = logging.getLogger("findadobe")

        paths = args
        if paths is not None and paths != "":
            buildFileSet(paths, destPath, extensions)
        else:
            print("Specify paths to search")
            sys.exit(1)

    except getopt.GetoptError:
        print("Error loading command line options")
        sys.exit(2)
