# File Deduplicator

This application is primarily built for processing media, like images, but the SIZE based approach is valid for any file type.

A couple examples of how to use this application are as follows:
When running a deduplication check over a single google takeout, the recommended parameters are:

--sortFolders=ASC --sortFiles=ASC --preferGoogleAblums --preferShorterNames
This basically tells the application when names are shorter keep img001.jpg vs img001(1).jpg

## Configuration Flags and Application Usage

--approach=[SIZE, EXIF]
This tells the application which approach to finding duplicates should be used.
When specifying SIZE, size is the first information checked, but it then moves on to actual file contents to compare when multiple files have the same size. This is HIGHLY accurate and very fast.

When specifying EXIF, the exif data is loaded from the images and then compared to all other files being processed. Sometimes image files have limited exif data stored in them, and because of this there is a HIGH likelihood to incorrectly identify duplicates. To reduce this risk, we also add a file name check. This is not quite perfect, but this is quite accurate under normal circumstances where files were copied between locations, or an image was resized or resampled, especially in the case of loading a file into Google Photos and an image gets downsampled. In this case the exif data does not get changed so this is a very accurate mechanism to detect duplicates.

The general recommendation is to deduplicate based on SIZE first then run the application again utilizing EXIF checks.

--preferFilesInFolder=[path to folder]
After a list of duplicates is built this is used to choose which file should be kept. If a single file in the list is in this folder it will be selected to keep. If there are more than one in that folder the other rules provided will take precedence for a selection (e.g. preferShortestName, etc...).

--sortFiles=[ASC, DESC]
This causes the files to be sorted based on file name, in either ascending or descending order. If not specified whatever order the operating systems returns the files is how they are ordered.

--sortFolders=[ASC, DESC]
This causes the folders to be sorted based on the full folder path, in either ascending or descending order. If not specified whatever order the operating systems returns the folders is how they are ordered.

--preferGoogleAlbums
By default when downloading a takeout from Google Photos, folders in an album named with a pattern Photos from YYYY. This flag tells the application to prefer files in folders that do NOT match that pattern.

--preferShorterNames
This tells the application when comparing files in set of duplicates, keep the file with the name that is shorter +img001.jpg -img001(1).jpg

--removeDuplicates (DANGER!)
Specifying this flag will DELETE files detected as duplicates. It is suggested to only use this flag after you are happy with the results of the duplicate check and preferences to organize the duplicates.

--extList=[comma separated list of extensions to process (no spaces)]
By default the application will scan all files in a folder. To limit the processing the file extension list can be limited e.g. JPG,MP4.

--logging=[path to logging.conf]

--help or -h
Displays the usage of the application
