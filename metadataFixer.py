#!/usr/bin/env python
"""
Fast duplicate file finder.
Usage: duplicates.py <folder> [<folder>...]
Based on https://stackoverflow.com/a/36113168/300783
Modified for Python3 with some small code improvements.
"""
import os
import sys
import re
import getopt
from time import time
from typing import NamedTuple, Optional, List, DefaultDict, Any
from collections import defaultdict
import logging.config
import logging
from pathlib import Path as Path
from datetime import datetime as _datetime
from datetime import timedelta as _timedelta
import piexif as _piexif
from fractions import Fraction  # piexif requires some values to be stored as rationals
import json
import pytz

logger = logging.getLogger()

TZ = pytz.timezone("UTC")
DEFAULT_HOUR = "17"
DEFAULT_MIN = "00"
DEFAULT_SEC = "00"
DEFAULT_DAY = "01"
DEFAULT_MONTH = "01"
PHOTO_FORMATS = [".jpg", ".jpeg", ".png", ".webp", ".bmp", ".tif", ".tiff", ".svg", ".heic"]
VIDEO_FORMATS = [".mp4", ".gif", ".mov", ".webm", ".avi", ".wmv", ".mpg", ".mpe", ".mpeg", ".mkv", ".m4v"]


def is_photo(file: Path):
    if file.suffix.lower() not in PHOTO_FORMATS:
        return False
    return True


def is_video(file: Path):
    if file.suffix.lower() not in VIDEO_FORMATS:
        return False
    return True


class FileDetail(NamedTuple):
    path: Path
    fullPath: str
    folder: str
    baseFileName: str
    extension: str
    fileName: str
    exifTimestamp: str
    folderTimestamp: str


FOLDER_YMD_PATTERN = ".*/([0-9]{4}/[0-9]{2}/[0-9]{2})/.*$"
FOLDER_YM_PATTERN = ".*/([0-9]{4}/[0-9]{2})/.*$"
FOLDER_Y_PATTERN = ".*/([0-9]{4})/.*$"

METADATA_FILENAME = "metadata.json"


def createJSON(timestamp: str, prettyDate: str):
    json = {
        "date": {"timestamp": timestamp, "formatted": prettyDate},
        "geoData": {"latitude": 0.0, "longitude": 0.0, "altitude": 0.0, "latitudeSpan": 0.0, "longitudeSpan": 0.0},
    }
    return json


def writeJSON(folder: str, timestamp: str):
    f = Path(folder)
    if f.is_dir():
        jsonFile = f.joinpath(METADATA_FILENAME)
        prettyDate = formatTimestamp(timestamp)
        jsonData = json.dumps(createJSON(timestamp, prettyDate), indent=2)

        # Writing to metadata.json
        with open(jsonFile, "w") as outfile:
            outfile.write(jsonData)


def parseFoldersForDate(folder: str) -> str:
    ts = None
    value = None
    ymd = re.search(FOLDER_YMD_PATTERN, folder)
    if ymd is not None:
        value = ymd.group(1)
    else:
        ym = re.search(FOLDER_YM_PATTERN, folder)
        if ym is not None:
            value = f"{ym.group(1)}/{DEFAULT_DAY}"
        else:
            y = re.search(FOLDER_Y_PATTERN, folder)
            if y is not None:
                value = f"{y.group(1)}/{DEFAULT_MONTH}/{DEFAULT_DAY}"

    if value is not None:
        value = f"{value} {DEFAULT_HOUR}:{DEFAULT_MIN}:{DEFAULT_SEC}"
        ts = getTimestamp(value)

    return ts


def buildFileSet(paths: List[str], writeMetadata: bool = False):
    baseSet: DefaultDict[str] = defaultdict(str)

    for path in paths:
        logger.info("Top Level: %s", path)
        for dirpath, _, filenames in os.walk(path):
            logger.debug("checking: %s", dirpath)
            for file in filenames:
                fullPath = os.path.join(dirpath, file)

                # if the target is a symlink (soft one), this will
                # dereference it - change the value to the actual target file
                fullPath = os.path.realpath(fullPath)
                base, _ = os.path.splitext(fullPath)
                folder, _ = os.path.split(base)
                if baseSet.get(folder) is None:
                    p = Path(fullPath)
                    if is_photo(p) or is_video(p):
                        exifTimestamp = None if not is_photo(p) else getTimestampFromEXIF(p)
                        if exifTimestamp is None:
                            folderTimestamp = parseFoldersForDate(folder + "/")
                            baseSet[folder] = folderTimestamp

    for folder in baseSet:
        ts = baseSet[folder]
        if writeMetadata:
            writeJSON(folder, ts)
        else:
            logger.info("Creating %s/metadata.json -- ts: %s -- date: %s", folder, ts, formatTimestamp(ts))


def formatTimestamp(timestamp):
    dt = _datetime(1970, 1, 1, tzinfo=TZ) + _timedelta(seconds=timestamp)
    return dt.strftime("%b %d, %Y, %I:%M:%S %p %Z")


def getTimestampFromEXIF(file: Path):
    try:
        # Why do you need to be like that, Piexif...
        exif_dict = _piexif.load(str(file))
    except Exception as e:
        raise IOError(f"Can't read file's exif! {file}")

    tags = [
        ["0th", _piexif.ImageIFD.DateTime],
        ["Exif", _piexif.ExifIFD.DateTimeOriginal],
        ["Exif", _piexif.ExifIFD.DateTimeDigitized],
    ]

    exifDateTime = ""
    timestamp = None
    for tag in tags:
        try:
            exifDateTime = exif_dict[tag[0]][tag[1]].decode("UTF-8")
            timestamp = getTimestamp(exifDateTime)
            break
        except KeyError:
            pass  # No such tag - continue searching :/
        except ValueError:
            pass  # keep on checking

    if timestamp is None:
        logger.debug("%s has has bad exif date format: %s, does not match 'Y:m:d H:M:S'", file, exifDateTime)

    return timestamp


def getTimestamp(exifDateTime: str):
    # Turns out exif can have different formats - YYYY:MM:DD, YYYY/..., YYYY-... etc
    # God wish that americans won't have something like MM-DD-YYYY
    # The replace ': ' to ':0' fixes issues when it reads the string as 2006:11:09 10:54: 1.
    # It replaces the extra whitespace with a 0 for proper parsing
    exifDateTime = (
        exifDateTime.replace("-", ":").replace("/", ":").replace(".", ":").replace("\\", ":").replace(": ", ":0")[:19]
    )
    v = _datetime.strptime(exifDateTime, "%Y:%m:%d %H:%M:%S")
    value = (v - _datetime(1970, 1, 1)).total_seconds()
    return int(value)


if __name__ == "__main__":
    loggingConfigFile = "logging.conf"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "w", ["logging", "help"])

        destPath = None
        writeFiles = False
        for opt, arg in opts:
            if opt in ("--logging"):
                loggingConfigFile = arg
                logger.debug("Logging configuration will be loaded from: '%s'", loggingConfigFile)
            elif opt in ("-w"):
                writeFiles = True

        #
        # Setup logging before doing any actual work
        #
        if not os.path.exists(loggingConfigFile):
            print(f"Logging configuration file does not exist: {loggingConfigFile}")
            sys.exit(3)

        logging.config.fileConfig(loggingConfigFile)
        logger = logging.getLogger("metadata-fixer")

        paths = args
        if paths is not None and paths != "":
            buildFileSet(paths, writeFiles)
        else:
            print("Specify paths to process")
            sys.exit(1)

    except getopt.GetoptError:
        print("Error loading command line options")
        sys.exit(2)
